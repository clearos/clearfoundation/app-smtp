If updating master.cf programatically, take into account that is it valid to split lines so this is valid:  
`smtps     inet  n       -       n       -       -       smtpd -o smtpd_tls_wrappermode=yes -o smtpd_sasl_auth_enable=yes`

But so is this:
```
smtps     inet  n       -       n       -       -       smtpd
    -o smtpd_tls_wrappermode=yes
    -o smtpd_sasl_auth_enable=yes
```
It is, therefore, safer to insert the line before another line rather than after.
